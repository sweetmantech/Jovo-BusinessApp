'use strict';

// ------------------------------------------------------------------
// APP INITIALIZATION
// ------------------------------------------------------------------

const { App } = require('jovo-framework');
const { Alexa } = require('jovo-platform-alexa');
const { GoogleAssistant,BasicCard } = require('jovo-platform-googleassistant');
const { JovoDebugger } = require('jovo-plugin-debugger');
const { FileDb } = require('jovo-db-filedb');
const { GoogleSheetsCMS } = require('jovo-cms-googlesheets');
const { DynamoDb } = require('jovo-db-dynamodb');
const { DashbotAlexa, DashbotGoogleAssistant } = require('jovo-analytics-dashbot');

const app = new App();

app.use(
    new Alexa(),
    new GoogleAssistant(),
    new JovoDebugger(),
    new FileDb(),
    new GoogleSheetsCMS(),
    new DynamoDb(),
    new DashbotAlexa(),
    new DashbotGoogleAssistant()
);

//FIREBASE INIT
const admin = require('firebase-admin');

var serviceAccount = require('./credentials/faq-firebase.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

var db = admin.firestore();
const rp = require('request-promise')
// Intent modules
var launch = require('./intents/Ann/launch')
var newBusiness = require('./intents/Ann/create.new.business.intent')
var storeLocation = require('./intents/Ann/add.store.location.intent')
var updateSkill = require('./intents/Ann/update.intent')
var publishSkill = require('./intents/Ann/publish.intent')
var deleteUser = require('./intents/Ann/deleteUser')
var accountLink = require('./intents/Ann/signin')
var sendLink = require('./intents/Ann/sendLink.intent')

// ------------------------------------------------------------------
// APP LOGIC
// ------------------------------------------------------------------
app.setHandler({
    NEW_SESSION(){
        if (this.isAlexaSkill()) {
            this.$session.$data.platform= "Amazon Alexa";
            this.$session.$data.platformApp = "Alexa Skill"
            this.$session.$data.device = "Amazon Echo"
        } else {
            this.$session.$data.platform = "Google Assistant"
            this.$session.$data.platformApp = "Action On Google"
            this.$session.$data.device = "Google Home"
        }
    },
    async LAUNCH() {
        await launch.launch(this,this.$session.$data.platform,this.$session.$data.platformApp,this.$session.$data.device) 
    },
    /**
     * CAN_FULFILL_INTENT - Triggered with Alexa CanFulfillIntentRequest
     */
    CAN_FULFILL_INTENT() {
        // first step - check if the incoming intent is one that our skill can handle.
        let intentName = this.getIntentName()
        let intentsWeCanFulfill = [ "CreateNewBusinessIntent", "AddStoreLocationIntent", "UpdateSkilllIntent","PublishSkillIntent","DeleteUserIntent","LearnMoreIntent","StartQuoteIntent"]
        if (intentsWeCanFulfill.includes(intentName)) {
            this.canFulfillRequest();
        } else {
            this.cannotFulfillRequest();
        }
    },
    WelcomeState :{
        WelcomeIntent() {
            this.$speech.addT('welcome.speech')
            this.$reprompt.addT('welcome.reprompt')
            // Visual card for Amazon Alexa and Google Assistant
            let title = 'Voice First';
            let content = 'say, "business card" to see our contact info';
            let imageUrl = 'https://s3.amazonaws.com/voice-first-business-app/logo.png';
            if(this.isGoogleAction()) {
                this.$googleAction.showSuggestionChips(['Create Business','Update App','Publish App', 'Help'])
            }
            this.showImageCard(title,content,imageUrl).ask(this.$speech,this.$reprompt)
        },
        YesIntent() {
            this.$speech.addT('welcome.yes.speech')
            this.$reprompt.addT('welcome.yes.reprompts')
            this.ask(this.$speech,this.$reprompt)
        },
        NoIntent() {
            this.toStatelessIntent('END')
        }
    },
    BusinessCardIntent() {
        this.$speech.addT('business.card.speech')
        this.$reprompt.addT('business.card.reprompt')
        if(this.isAlexaSkill()) {
            this.$alexaSkill.showStandardCard('Voice First Tech','Office#: 513-850-5895',{
                smallImageUrl:'https://odesk-prod-portraits.s3.amazonaws.com/Companies:5209706:CompanyLogoURL?AWSAccessKeyId=AKIAIKIUKM3HBSWUGCNQ&Expires=2147483647&Signature=C1yx2OBhlD3LErBSbQpMrZbX2iE%3D',
                largeImageUrl:'https://odesk-prod-portraits.s3.amazonaws.com/Companies:5209706:CompanyLogoURL?AWSAccessKeyId=AKIAIKIUKM3HBSWUGCNQ&Expires=2147483647&Signature=C1yx2OBhlD3LErBSbQpMrZbX2iE%3D',
            })
            this.ask(this.$speech,this.$reprompt)
        } else {
            let basicCard = new BasicCard()
            .setTitle('Voice First Tech')
            .setFormattedText('Office#: 513-850-5895')
            .setImage({
                url: "https://odesk-prod-portraits.s3.amazonaws.com/Companies:5209706:CompanyLogoURL?AWSAccessKeyId=AKIAIKIUKM3HBSWUGCNQ&Expires=2147483647&Signature=C1yx2OBhlD3LErBSbQpMrZbX2iE%3D",
                accessibilityText: 'Business Card',
                width: 350,
                height: 150
            })
            .addButton('Voice First Tech website', 'https://www.VoiceFirstTech.com/')
            this.$googleAction.showSuggestionChips(['Create Business','Update App','Learn More', 'Price Quote', 'Help','Add Location'])
        this.$googleAction.showBasicCard(basicCard).ask(this.$speech,this.$reprompt)
        }
            

    },
    async BetaTestIntent() {
        await betaTest.betaTester(this)
    },
    QuoteIntent() {
        this.toStateIntent('QuoteState','StartQuoteIntent')
    },
    LearnMoreIntent() {
        this.$speech.addT('learnmore.speech')
        this.$reprompt.addT('learnmore.reprompt')
        // Visual card for Amazon Alexa and Google Assistant
        let title = 'Voice First';
        let content = 'say, "business card" to see our contact info';
        let imageUrl = 'https://s3.amazonaws.com/voice-first-business-app/logo.png';
        if(this.isGoogleAction()) {
            this.$googleAction.showSuggestionChips(['Create Business','Update App','Publish App', 'Help'])
        }        this.showImageCard(title, content, imageUrl)
            .ask(this.$speech, this.$reprompt);
    },
    async CreateNewBusinessIntent() { await newBusiness.createNewBusiness(this, this.$session.$data.platformApp)},

    CreateNewBusinessState : {
        async CreateNewBusinessIntent() { await newBusiness.createNewBusiness(this, this.$session.$data.platformApp) },  
        Unhandled () { newBusiness.unhandled(this) },
        HelpIntent() { newBusiness.help(this) },
        async DeleteUserIntent() { await deleteUser.delete(this) },
        END () {
            return this.tell('Ok, Goodbye')
        }
    },

    async AddStoreLocationIntent() { await storeLocation.addStoreLocation(this) },

    AddStoreLocationState: {
        async AddStoreLocationIntent() { await storeLocation.addStoreLocation(this) },
        Unhandled () { storeLocation.unhandled(this) },
        END () {
            return this.tell('Ok, Goodbye')
        }
    },
    async UpdateSkillIntent () { await updateSkill.update(this) },

    async PublishSkillIntent () { await publishSkill.publish(this) },

    async SignInIntent () {await accountLink.signin(this,this.$session.$data.platform,this.$session.$data.device)},
    
    async ON_SIGN_IN() { 
        if (this.$googleAction.getSignInStatus() === 'OK') {
            let token = that.$request.getAccessToken();
            let options = {
                method: 'GET',
                uri: 'https://voicefirsttech.auth0.com/userinfo',
                headers: {
                    authorization: 'Bearer ' + token,
                }
            };
            await rp (options).then((body)=> {
                let data = JSON.parse(body)
                console.log(data)
                that.$session.$data.email = data.email
            })
            that.$speech.addText(that.t('existing.business.lookup.intent.no_name.prompt'))
            that.$reprompt.addText(that.t('existing.business.lookup.intent.no_name.reprompt'))
            return that.followUpState('CreateNewBusinessState').ask(that.$speech,that.$reprompt)
        }else {
            this.tell('There was an error. We could not sign in you in.');
        }
    },
    UpdateSkillState : {
        async UpdateSkillIntent () { await updateSkill.update(this) },
        Unhandled()  { updateSkill.unhandled(this) },
        HelpIntent() { updateSkill.help(this) },
        END () {
            return this.tell('Ok, Goodbye')
        }
    },
    PublishSkillState : {
        async PublishSkillIntent () { await publishSkill.publish(this) },
        Unhandled()  { publishSkill.unhandled(this) },
        HelpIntent() { publishSkill.help(this) },
        END () {
            return this.tell('Ok, Goodbye')
        }
    },
    async DeleteUserIntent() { await deleteUser.delete(this) },

    async SendLinkIntent() {await sendLink.sendLink(this, db)},
    SendLinkState: {
        async SendLinkIntent() {await sendLink.sendLink(this, db)},
        Unhandled()  {sendLink.unhandled(this)},
        HelpIntent() {sendLink.help(this)}
    },
    END () {
        return this.tell('Ok, Goodbye')
    },
    HelpIntent() {
        this.$speech.addT('help.global.speech')
        this.ask(this.$speech)
    },
    Unhandled() {
        this.$speech.addT('unhandled.global.speech')
        if (this.isGoogleAction()){
            this.$googleAction.showSuggestionChips(['Business Card', 'Learn More', 'Price Quote', 'Help','Create Business','Update App','Add Location']);

        }
        this.ask(this.$speech);
    },
    QuoteState: {
        StartQuoteIntent() {
            this.$speech.addT('quote.speech')
            this.$reprompt.addT('quote.reprompt')
            // Visual card for Amazon Alexa and Google Assistant
            let title = 'Pricing Buddy - Coming Soon';
            let content = 'Quote coming soon! Get pricing to have Voice First build your voice app for Amazon Alexa and Google Assistant';
            let imageUrl = 'https://s3.amazonaws.com/voice-first-business-app/logo.png';
            if (this.isGoogleAction()) {
            this.$googleAction.showLinkOutSuggestion('Voice First', 'https://www.voicefirsttech.com/');
            this.$googleAction.showSuggestionChips(['Create Business','Update App','Publish App', 'Learn More', 'Help']);
            }
            this.showImageCard(title, content, imageUrl)
                .ask(this.$speech, this.$reprompt);
        }
    }
    
    
});

// arn:aws:lambda:us-east-1:604715680480:function:VoiceFirstAI_Ann_v2_PUBLISH

module.exports.app = app;
