const fx = require('./helperFx')
let data = {}
var addStoreUrl = "https://us-central1-frequently-asked-questio-52310.cloudfunctions.net/addStoreLocation"
module.exports = {
    addStoreLocation : async function (that) {
        let businessName = that.$inputs.businessName ? that.$inputs.businessName.value : undefined
        let location = that.$inputs.addressLine ? that.$inputs.addressLine.value :undefined
        let city = that.$inputs.city ? that.$inputs.city.value : undefined
        if(!that.$user.$data.businessName && !businessName) {
            that.$speech.addT('location.businesssName.prompt')
            that.$reprompt.addT('location.businesssName.reprompt')
            that.followUpState('AddStoreLocationState').ask(that.$speech, that.$reprompt)
        } else if (!location || !city) {
            that.$user.$data.businessName=(that.$user.$data.businessName||businessName)
            if(!await fx.databaseCheck(that.$user.$data.businessName)){
                that.$speech.addT('location.no_skill.prompt')
                that.$reprompt.addT('location.no_skill.reprompt')
                that.followUpState('CreateNewBusinessState').ask(that.$speech,that.$reprompt)
            }else{
                that.$speech.addT(that.t('location.none.prompt'))
                that.$reprompt.addT(that.t('location.none.reprompt'))
                that.followUpState('AddStoreLocationState').ask(that.$speech, that.$reprompt)
            }
        } else {
            let address = location + "," + city
            data = {
                "name":that.$user.$data.businessName,
                "address":address
            }
            await fx.apiRequest(addStoreUrl,data)
            that.$speech.addT('Ok! Location has been added')
            that.tell(that.$speech)
        }
    },
    unhandled : function (that) {
        that.$speech.addT(that.t('location.unhandled.prompt'))
        that.$reprompt.addT(that.t('location.unhandled.reprompt'))
        that.followUpState('AddStoreLocationState').ask(that.$speech, that.$reprompt)
    }
}
