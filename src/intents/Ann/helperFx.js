const admin = require('firebase-admin');
var db = new admin.firestore();
const request = require('request')

module.exports = {
    /**
     * DatabaseCheck - searches Cloud Firestore database to see if document exists and returns boolean value
     * @param {*} businessName - name of the business/app
     */
    databaseCheck: async function (businessName) {
        let check;
        let businessRef = db.collection('business').doc(businessName)
        await businessRef.get()
            .then(async function (doc){
                if(!doc.exists) {
                    check = false
                }else {
                    check = true
                }
            })
        return check
    },
    /**
     * apiRequest - posts data to specified api url 
     * @param {*} url - api url to send request 
     * @param {*} data - data object to send to api
     */
    apiRequest:async function (url,data){
        await request.post(url,{
            json:data
        },async(error,res,body)=>{
            if (error) {
                await console.log(error)
                return
            }
            await console.log(body)
        })
    },
    /**
     * updateGitLabRequest - triggers gitlab pipeline to update corresponding app
     * @param {*} businessName - name of the business/app
     */
    updateGitLabRequest:async function (businessName) {
        await request.post('https://gitlab.com/api/v4/projects/12054268/trigger/pipeline', 
                        {
                            form:{
                                token:'0d37c7214db88ef2dd71998d7fbff6',
                                ref: 'feature/voice',
                                'variables[UPDATE]':true,
                                'variables[NAME]': businessName
                            }
                        }
                    )
    },
    /**
     * publishGitlabRequest - triggers gitlab pipeline to publish corresponding app
     * @param {*} businessName-name of the business/app
     */
    publishGitlabRequest:async function(businessName) {
        await request.post('https://gitlab.com/api/v4/projects/12054268/trigger/pipeline', 
                        {
                            form:{
                                token:'0d37c7214db88ef2dd71998d7fbff6',
                                ref: 'feature/voice',
                                'variables[PUBLISH]':true,
                                'variables[NAME]': businessName
                            }
                        }
                    )
    },
    /**
     * updates- gets the corresponding update code stored in database
     * @param {*} businessName - name of the business/app
     * @return - update code in database for corresponding app/business
     */
    updates:async function(businessName) {
        let updateCode
        await db.collection('business').doc(businessName).get()
            .then (async function (doc) {
                let newValue = await doc.data()
                if (newValue.d){
                    updateCode = newValue.d.updateCode  
                } else {
                    updateCode = newValue.updateCode
                }
            })
        return updateCode
    }
}
