module.exports = {
    delete: async function (that) {
        // Delete User
        console.log("DeleteItem succeeded:");
        // DELIVER RESULTS
        if (that.isAlexaSkill()) {
            that.$alexaSkill.progressiveResponse('Deleting')
                .then(() => that.$alexaSkill.progressiveResponse('Still deleting'));
        }
        that.$speech.addText('Your information has been deleted');
        await that.$user.delete(that.tell(that.$speech));
    }
}
