const fx = require('./helperFx')
module.exports = {
    publish : async function (that) {
        let businessName = (that.$inputs.businessName.value || that.$user.$data.businessName||undefined)
        let updateCode = (that.$inputs.code.value || undefined)
        console.log(that.$user.$data.day)
        console.log(that.$user.$data.businessName)
        if (!that.$user.$data.businessName && !businessName) {
            that.$speech.addT('publishCode.noName.prompt')
            that.$reprompt.addT('publishCode.noName.reprompt')
            that.followUpState('PublishSkillState').ask(that.$speech, that.$reprompt)
        }else if (that.$user.$data.day === 4 ) {
            that.$speech.addT('day.four.complete.prompt')
            await fx.publishGitlabRequest(that.$user.$data.businessName)
            that.$user.$data.day = 4
            that.tell(that.$speech)
        }else if (!updateCode && that.isAlexaSkill()) {
            that.$user.$data.businessName=(that.$user.$data.businessName||businessName)
            if (!await fx.databaseCheck(that.$user.$data.businessName)){
                that.$speech.addT('publishCode.no_business.prompt', {business: that.$user.$data.businessName})
                that.$reprompt.addT('publishCode.no_business.reprompt', {business: that.$user.$data.businessName})
                that.followUpState('CreateNewBusinessState').ask(that.$speech, that.$reprompt)
            }else {
                that.$session.$data.updateCode = await fx.updates(that.$user.$data.businessName)
                that.$speech.addT('publishCode.none.prompt')
                that.$reprompt.addT('publishCode.none.reprompt')
                that.followUpState('PublishSkillState').ask(that.$speech, that.$reprompt)
            }
        }else if (that.isGoogleAction()) {
            that.$user.$data.businessName=(that.$user.$data.businessName||businessName)
            if (!await fx.databaseCheck(that.$user.$data.businessName)){
                that.$speech.addT('publishCode.no_business.prompt')
                that.$reprompt.addT('publishCode.no_business.reprompt')
                that.followUpState('CreateNewBusinessState').ask(that.$speech, that.$reprompt)
            }else {
                await fx.publishGitlabRequest(that.$user.$data.businessName)    
                await that.$user.delete(that.tell('Success, ' +  that.$user.$data.businessName + ' will be published in a few. Thank you for using Voice First A I!'))
            }

        } else if ( updateCode === parseInt(that.$session.$data.updateCode) && that.isAlexaSkill()) {
            await fx.publishGitlabRequest(that.$user.$data.businessName)    
            await that.$user.delete(that.tell('Success, ' +  that.$user.$data.businessName + ' will be published in a few. Thank you for using Voice First A I!'))
        } else {
            that.$speech.addT('publishCode.wrong.prompt')
            that.$reprompt.addT('publishCode.wrong.reprompt')
            that.followUpState('PublishSkillState').ask(that.$speech, that.$reprompt)
        }
    },
    unhandled : function (that) {
        if(!that.$user.$data.businessName) {
            that.$speech.addT('publishCode.unhandled.name.prompt')
            that.$reprompt.addT('publishCode.unhandled.name.reprompt')
        } else {
            that.$speech.addT('publishCode.unhandled.code.prompt')
            that.$reprompt.addT('publishCode.unhandled.code.reprompt')
        }
        that.followUpState('PublishSkillState').ask(that.$speech,that.$reprompt)
    },
    help : function (that) {
        if (!that.$user.$data.businessName) {
            that.$speech.addT('publishCode.businessName.help.prompt')
            that.$reprompt.addT('publishCode.businessName.help.reprompt')
        } else {
            that.$speech.addT('publishCode.updateCode.help.prompt')
            that.$reprompt.addT('publishCode.updateCode.help.reprompt')
        }
        that.followUpState('PublishSkillState').ask(that.$speech, that.$reprompt)
    }
}
