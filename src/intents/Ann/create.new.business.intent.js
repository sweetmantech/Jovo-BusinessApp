const fx = require('./helperFx')//Helper functions to make code easier to read
let data = {}
var createBusinessUrl = "https://us-central1-frequently-asked-questio-52310.cloudfunctions.net/createBusiness"
var createBusinessDay1Url = "https://us-central1-frequently-asked-questio-52310.cloudfunctions.net/createBusinessDay1"

module.exports = {
    createNewBusiness : async function (that,platformApp) {
        let phone = (that.$inputs.phoneNumber.key || that.$user.$data.phone || undefined)
        let businessName = (that.$inputs.businessName.value || that.$user.$data.businessName || undefined)
        if(!businessName) {
            that.$speech.addText(that.t('existing.business.lookup.intent.no_name.prompt'))
            that.$reprompt.addText(that.t('existing.business.lookup.intent.no_name.reprompt'))
            that.followUpState('CreateNewBusinessState').ask(that.$speech,that.$reprompt)
        } else if(!phone) {
            that.$user.$data.businessName=(that.$user.$data.businessName||businessName)
            if (!await fx.databaseCheck(that.$user.$data.businessName)){
                if (that.$user.$data.day === 1) {
                    that.$speech.addT('day.one.phone.prompt',{platformApp:platformApp})
                    that.$reprompt.addT('day.one.phone.reprompt')
                }else{
                    that.$speech.addText(that.t('create.new.business.no_phone.prompt',{businessName:that.$user.$data.businessName}))
                    that.$reprompt.addText(that.t('create.new.business.no_phone.reprompt'))
                }
                await that.followUpState('CreateNewBusinessState').ask(that.$speech,that.$reprompt)
            }
            else {
                that.$speech.addText(that.t('existing.business.prompt'))
                that.$reprompt.addText(that.t('existing.business.reprompt'))
                await that.removeState().ask(that.$speech,that.$reprompt)
            }          
        } else {
            that.$user.$data.phone = (that.$user.$data.phone || phone)
            if (that.$user.$data.day ===1) {
                that.$speech.addT('day.one.complete.prompt')
                that.$user.$data.day = 2
                data = {
                    "name":that.$user.$data.businessName,
                    "phone":that.$user.$data.phone 
                }
                await fx.apiRequest(createBusinessDay1Url,data)
            }else {
                that.$speech.addText('Great! we will notify you when your app has been created')
                let update = Math.floor(Math.random()*90000) + 10000;
                that.$user.$data.updateCode = update 
                data = {
                    "name":that.$user.$data.businessName,
                    "phone":that.$user.$data.phone,
                    "email":that.$user.$data.email,
                    "updateCode":that.$user.$data.updateCode  
                } 
                await fx.apiRequest(createBusinessUrl,data)        
            }
            that.tell(that.$speech)
        }  
    },
    unhandled : function (that) {
        if (!that.$user.$data.businessName){
            that.$speech.addT(that.t('new.business.unhandled.prompt'))
            that.$reprompt.addT(that.t('new.business.unhandled.reprompt'))
        }else {
            that.$speech.addT('new.business.unhandled.phone.prompt')
            that.$reprompt.addT('new.business.unhandled.phone.reprompt')
        }
        that.followUpState('CreateNewBusinessState').ask(that.$speech,that.$reprompt)
    },
    help : function (that) {
        if (!that.$user.$data.businessName) {
            that.$speech.addT('createBusiness.businessName.help.prompt')
            that.$reprompt.addT('createBusiness.businessName.help.reprompt')
        }else {
            that.$speech.addT('createBusiness.businessPhone.help.prompt')
            that.$reprompt.addT('createBusiness.businessPhone.help.reprompt')
        }
        that.followUpState('CreateNewBusinessState').ask(that.$speech, that.$reprompt)
    }
}
