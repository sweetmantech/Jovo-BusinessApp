module.exports = {
    launch : async function (that,platform) {
        if(that.$user.isNew()) {
            that.$user.$data.day = 1
            if (that.isAlexaSkill()) {
                that.$speech.addT('welcome.newUser.alexa.speech',{platform:platform})
            } else {
                that.$speech.addT('welcome.newUser.google.speech',{platform:platform})
            }
            that.$reprompt.addT('welcome.newUser.reprompt')
            that.followUpState('CreateNewBusinessState').ask(that.$speech,that.$reprompt)
        } else if(that.$user.$data.day === 2) {
            return that.toStatelessIntent('BetaTestIntent')
        } else if(that.$user.$data.day === 3 ) {
            that.$speech.addT('day.three.welcome.prompt')
            that.$reprompt.addT('day.three.welcome.reprompt')
            that.ask(that.$speech, that.$reprompt)
        }else if (that.$user.$data.day === 4 ) {
            that.$speech.addT('day.four.welcome.prompt',{platform:platform})
            that.$reprompt.addT('day.four.welcome.reprompt')
            that.ask(that.$speech,that.$reprompt)
        } else{
            return that.toStateIntent('WelcomeState','WelcomeIntent')
        } 
    }
}