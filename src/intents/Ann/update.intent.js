const fx = require('./helperFx')
module.exports = {
    update : async function (that) {
        let businessName = (that.$inputs.businessName.value || that.$user.$data.businessName||undefined)
        let updateCode = (that.$inputs.code.value || undefined)

        if (!that.$user.$data.businessName && !businessName) {
            that.$speech.addT('updateCode.noName.prompt')
            that.$reprompt.addT('updateCode.noName.reprompt')
            that.followUpState('UpdateSkillState').ask(that.$speech, that.$reprompt)
        }else if (that.$user.$data.day === 3 ) {
            that.$speech.addT('day.three.complete.prompt')
            await fx.updateGitLabRequest(that.$user.$data.businessName)
            that.$user.$data.day = 4
            that.tell(that.$speech)
        }else if (!updateCode && that.isAlexaSkill()) {
            that.$user.$data.businessName=(that.$user.$data.businessName||businessName)
            if (!fx.databaseCheck(that.$user.$data.businessName)){
                that.$speech.addT('updateCode.no_business.prompt')
                that.$reprompt.addT('updateCode.no_business.reprompt')
                that.followUpState('CreateNewBusinessState').ask(that.$speech, that.$reprompt)
            }else {
                that.$session.$data.updateCode = await fx.updates(that.$user.$data.businessName)
                that.$speech.addT('updateCode.none.prompt')
                that.$reprompt.addT('updateCode.none.reprompt')
                that.followUpState('UpdateSkillState').ask(that.$speech, that.$reprompt)
            }
        }else if (!that.isAlexaSkill()) {
            that.$user.$data.businessName=(that.$user.$data.businessName||businessName)
            if (!fx.databaseCheck(that.$user.$data.businessName)){
                that.$speech.addT('updateCode.no_business.prompt')
                that.$reprompt.addT('updateCode.no_business.reprompt')
                that.followUpState('CreateNewBusinessState').ask(that.$speech, that.$reprompt)
            }else {
                await fx.updateGitLabRequest(that.$user.$data.businessName)
                await that.$user.delete(that.tell('Great, Your app will be updated in a few'))
            }
        } else if ( toString(updateCode) === toString(that.$session.$data.updateCode) && that.isAlexaSkill()) {
            await fx.updateGitLabRequest(that.$user.$data.businessName)    
            await that.$user.delete(that.tell('Great, Your app will be updated in a few. Thank you for using Voice First A I!'))
        } else {
            that.$speech.addT('updateCode.wrong.prompt')
            that.$reprompt.addT('updateCode.wrong.reprompt')
            that.followUpState('UpdateSkillState').ask(that.$speech, that.$reprompt)
        }
    },
    unhandled : function (that) {
        if(!that.$user.$data.businessName) {
            that.$speech.addT('updateCode.unhandled.name.prompt')
            that.$reprompt.addT('updateCode.unhandled.name.reprompt')
        } else {
            that.$speech.addT('updateCode.unhandled.code.prompt')
            that.$reprompt.addT('updateCode.unhandled.code.reprompt')
        }
        that.followUpState('UpdateSkillState').ask(that.$speech,that.$reprompt)
    },
    help : function (that) {
        if (!that.$user.$data.businessName) {
            that.$speech.addT('updateSkill.businessName.help.prompt')
            that.$reprompt.addT('updateSkill.businessName.help.reprompt')
        } else {
            that.$speech.addT('updateSkill.updateCode.help.prompt')
            that.$reprompt.addT('updateSkill.updateCode.help.reprompt')
        }
        that.followUpState('UpdateSkillState').ask(that.$speech, that.$reprompt)
    }
}
