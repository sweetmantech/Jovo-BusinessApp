const fx = require('./helperFx')
// TWILIO INIT
const accountSid = "AC75d3d592cc5a6e785bd374de7dd7b44e";
const authToken = "926d8684e95b04512c3c1939dcee7dfa";
const client = require('twilio')(accountSid, authToken);
const twilioNumber = '+16143003104'

module.exports = {
    sendLink : async function(that, db){
        let businessName = (that.$inputs.businessName.value || that.$user.$data.businessName || undefined)
        if(!businessName) {
            that.$speech.addT('sendLink.noName.prompt')
            that.$reprompt.addT('sendLink.noName.reprompt') 
            that.followUpState('SendLinkState').ask(that.$speech, that.$reprompt)
        } else {
            that.$user.$data.businessName = (that.$user.$data.businessName || businessName)
            console.log(await fx.databaseCheck(that.$user.$data.businessName))
            if(await fx.databaseCheck(businessName)) {
            let sheetUrl = await sheetURL(db,businessName)
            body = "Here is the link to your googlesheets " + sheetUrl.sheetURL
            await client.messages.create({
                body: body,
                to: sheetUrl.phone,
                from: twilioNumber
            })
           } else {
            that.$speech.addText(that.t('create.new.business.no_phone.prompt',{businessName:that.$user.$data.businessName}))
            that.$reprompt.addText(that.t('create.new.business.no_phone.reprompt'))
            await that.followUpState('CreateNewBusinessState').ask(that.$speech,that.$reprompt)

           }
        }
    },
    unhandled : function(that) {
            that.$speech.addT('sendLink.business.unhandled.prompt')
            that.$reprompt.addT('sendLink.business.unhandled.reprompt')
            that.followUpState('SendLinkState').ask(that.$speech,that.$reprompt)
    },
    help : function(that) {
            that.$speech.addT('sendLink.businessName.help.prompt')
            that.$reprompt.addT('sendLink.businessName.help.reprompt')
            that.followUpState('SendLinkState').ask(that.$speech, that.$reprompt)
    }
}

async function sheetURL(geocollection,businessName) {
    let update = {}
     await geocollection.collection('business').doc(businessName).get()
         .then (async function (doc) { 
                let newValue = await doc.data()
                if (newValue.d) {
                update.phone = (newValue.d.phone || "No Phone")
                update.sheetURL = (newValue.sheetURL || "No Sheet URL")
                } else {
                 update.phone = (newValue.phone || "No Phone")
                 update.sheetURL = (newValue.sheetURL || "No Sheet URL")
             }
         })
     return update
 }
