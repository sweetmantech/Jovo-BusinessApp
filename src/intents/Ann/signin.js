module.exports = {
    signin: (that,platform,device) => {
        that.showAccountLinkingCard()
        if (that.isAlexaSkill()) {
            if (that.$user.$data.day === 2) {
                that.$speech.addT('day.two.welcome.prompt',{business:that.$user.$data.businessName,platform:platform,device:device})
                that.tell(that.$speech)
            }else {
                that.$speech.addText('LinkAccount card is sent in the home section of the alexa app, please go the Alexa companion app and link your account so that we may add you as a beta tester, for now you can say "learn more"');
                that.$reprompt.addText('LinkAccount card is sent in the home section of the alexa app, please go the Alexa companion app and link your account');
                that.ask(that.$speech, that.$reprompt);
            }
        } else {
            return that.toStatelessIntent('ON_SIGN_IN')
        }
        
    }
}
