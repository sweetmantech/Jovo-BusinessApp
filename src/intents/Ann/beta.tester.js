
const admin = require('firebase-admin');
var db = new admin.firestore();
const rp = require('request-promise')
const fx = require('./helperFx')
let data = {}
const betaFxUrl = 'https://us-central1-frequently-asked-questio-52310.cloudfunctions.net/addBetaTester'
module.exports = {
    betaTester: async function(that) {
        if (!that.$request.getAccessToken()) {
            return that.toStatelessIntent("SignInIntent")
        } else {
            let token = that.$request.getAccessToken();
            let options = {
                method: 'GET',
                uri: 'https://voicefirsttech.auth0.com/userinfo',
                headers: {
                    authorization: 'Bearer ' + token,
                }
            };
            await rp (options).then((body)=> {
                let data1 = JSON.parse(body)
                console.log(data1)
                that.$session.$data.email = data1.email
            })
            await db.collection('business').doc(that.$user.$data.businessName).update({email:that.$session.$data.email})
            data = {
                "name": that.$user.$data.businessName,
                "email": that.$session.$data.email
            }
            await fx.apiRequest(betaFxUrl,data)
            that.$user.$data.day = 3
            that.$speech.addT('day.two.complete.prompt')
            that.tell(that.$speech)
        }
    }
}