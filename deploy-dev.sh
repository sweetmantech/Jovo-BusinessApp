#!/bin/bash
# ONLY RUN FROM LOCAL MACHINE
# This script updates the provided app
# Updates Skill Model
# Updates Skill Manifest (info)
# Updates Dev Lambda
# run with the command:
# sudo sh deploy-dev.sh "<name-of-cleaner>"
python sheets.py -b "$1"
jovo build -d --stage dev