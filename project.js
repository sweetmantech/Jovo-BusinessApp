// ------------------------------------------------------------------
// JOVO PROJECT CONFIGURATION
// ------------------------------------------------------------------
var skillId = "amzn1.ask.skill.01eb916d-1379-431c-b9e9-e928fdde84af";
var devLambda = "arn:aws:lambda:us-east-1:604715680480:function:VoiceFirstAI_Ann_Dev"
var prodLambda = "arn:aws:lambda:us-east-1:604715680480:function:VoiceFirstAI_Ann_v2_PUBLISH";
var googleApiGatewayUrl = "https://gvl47pn348.execute-api.us-east-1.amazonaws.com/PROD-GOOGLE"
var business = "Voice First AI";
var smallLogo = "https://firebasestorage.googleapis.com/v0/b/voice-first-tech.appspot.com/o/logo%2Flogo_108x108.png?alt=media"
var largeLogo = "https://firebasestorage.googleapis.com/v0/b/voice-first-tech.appspot.com/o/logo%2Flogo_512x512.png?alt=media"
 
module.exports = {
    endpoint: '${JOVO_WEBHOOK_URL}',
    stages:{
        local:{
            alexaSkill: {
                nlu: 'alexa',
                skillId: skillId, // voice dry cleaner dot com Skill ID
                manifest: {
                    publishingInformation: {
                        locales: {
                            "en-US":{
                                summary: "Voice First AI",
                                description: "Voice First AI allows users to learn more about the voice building company based out of Ohio. This skill also allows anyone to create a simple faq app using their voice to quickly get on the voice platform",
                                examplePhrases: [
                                    "Alexa open " + business + " ",
                                    "Alexa open " + business + " and create my business",
                                    "Alexa open " + business + "and add my business to voice"
                                ],
                                keywords: [
                                    "voice first",
                                    "voice first ai",
                                    "Voice first tech",
                                    "create business",
                                    "business voice creator",
                                    "USA",
                                    "ohio",
                                    "voice",
                                    business,
                                    "create alexa skill",
                                    "skill creator",
                                    "create google action",
                                    "alexa for business",
                                    "voice by voice"
                                ],
                                smallIconUri: smallLogo,
                                largeIconUri: largeLogo,
                                name: business
                            },
                        },
                        isAvailableWorldwide: true,
                        testingInstructions: "1) say Alexa, open " + business+ " ",
                        category: "SMART_HOME",
                        distributionCountries: []
                    },
                    apis: {
                        custom: {
                            interfaces: [
                                {
                                    type: 'CAN_FULFILL_INTENT_REQUEST',
                                },
                                {
                                    type: 'ALEXA_PRESENTATION_APL'
                                }
                            ]
                        }
                    },
                    permissions: [
                        {
                            name: 'alexa::devices:all:notifications:write'
                        }
                    ],
                    "privacyAndCompliance": {
                        "allowsPurchases": false,
                        "usesPersonalInfo": false,
                        "isChildDirected": false,
                        "isExportCompliant": true,
                        "containsAds": false,
                        "locales": {
                            "en-US": {
                                "privacyPolicyUrl": "http://voicefirsttech.com/my-dry-cleaner/privacy-policy.pdf",
                                "termsOfUseUrl": "https://voicefirsttech.com/my-dry-cleaner/terms-of-use.pdf"
                            }
                        }
                    },
                },
            },
            googleAction: {
                nlu: 'dialogflow',
                dialogflow:{
                    projectId: "voicefirsttech-mybusiness",
                    keyFile: "voicefirsttech-mybusiness-dialogflow.json",
                    agent: {
                        webhook:{
                            url: googleApiGatewayUrl
                        }
                    }
                }      
                
            }
        },
        dev: {
            alexaSkill: {
                nlu: 'alexa',
                skillId: skillId, // voice dry cleaner dot com Skill ID
                endpoint: devLambda, // Dev Lambda ARN
                manifest: {
                    publishingInformation: {
                        locales: {
                            "en-US":{
                                summary: "Voice First AI",
                                description: "Voice First AI allows users to learn more about the voice building company based out of Ohio. This skill also allows anyone to create a simple faq app using their voice to quickly get on the voice platform",
                                examplePhrases: [
                                    "Alexa open " + business + " ",
                                    "Alexa open " + business + " and create my business",
                                    "Alexa open " + business + "and add my business to voice"
                                ],
                                keywords: [
                                    "voice first",
                                    "voice first ai",
                                    "Voice first tech",
                                    "create business",
                                    "business voice creator",
                                    "USA",
                                    "ohio",
                                    "voice",
                                    business,
                                    "create alexa skill",
                                    "skill creator",
                                    "create google action",
                                    "alexa for business",
                                    "voice by voice"
                                ],
                                smallIconUri: smallLogo,
                                largeIconUri: largeLogo,
                                name: business
                            },
                        },
                        isAvailableWorldwide: true,
                        testingInstructions: "1) say Alexa, open " + business+ " " + "2) create my business 3) To test account linking use test_account@gmail.com as the username and test_account1 as the password 4) say any business name plus the word test",
                        category: "SMART_HOME",
                        distributionCountries: []
                    },
                    apis: {
                        custom: {
                            endpoint: {
                                uri: devLambda
                            },
                            interfaces: [
                                {
                                    type: 'CAN_FULFILL_INTENT_REQUEST',
                                },
                                {
                                    type: 'ALEXA_PRESENTATION_APL'
                                }
                            ]
                        }
                    },
                    permissions: [
                        {
                            name: 'alexa::devices:all:notifications:write'
                        }
                    ],
                    "privacyAndCompliance": {
                        "allowsPurchases": false,
                        "usesPersonalInfo": false,
                        "isChildDirected": false,
                        "isExportCompliant": true,
                        "containsAds": false,
                        "locales": {
                            "en-US": {
                                "privacyPolicyUrl": "http://voicefirsttech.com/my-dry-cleaner/privacy-policy.pdf",
                                "termsOfUseUrl": "https://voicefirsttech.com/my-dry-cleaner/terms-of-use.pdf"
                            }
                        }
                    },
                },
            },
            googleAction: {
                nlu: 'dialogflow',
                dialogflow:{
                    projectId: "voicefirsttech-mybusiness",
                    keyFile: "voicefirsttech-mybusiness-dialogflow.json",
                    agent: {
                        webhook:{
                            url: googleApiGatewayUrl
                        }
                    }
                }      
                
            }
        },
        prod: {
            alexaSkill: {
                nlu: 'alexa',
                skillId: skillId, // voice dry cleaner dot com Skill ID
                endpoint: prodLambda, // Dev Lambda ARN
                manifest: {
                    publishingInformation: {
                        locales: {
                            "en-US":{
                                summary: "Voice First AI",
                                description: "Voice First AI allows users to learn more about the voice building company based out of Ohio. This skill also allows anyone to create a simple faq app using their voice to quickly get on the voice platform",
                                examplePhrases: [
                                    "Alexa open " + business + " ",
                                    "Alexa open " + business + " and create my business",
                                    "Alexa open " + business + "and add my business to voice"
                                ],
                                keywords: [
                                    "voice first",
                                    "voice first ai",
                                    "Voice first tech",
                                    "create business",
                                    "business voice creator",
                                    "USA",
                                    "ohio",
                                    "voice",
                                    business,
                                    "create alexa skill",
                                    "skill creator",
                                    "create google action",
                                    "alexa for business",
                                    "voice by voice"
                                ],
                                smallIconUri: smallLogo,
                                largeIconUri: largeLogo,
                                name: business
                            },
                        },
                        isAvailableWorldwide: true,
                        testingInstructions: "1) say Alexa, open " + business+ " ",
                        category: "SMART_HOME",
                        distributionCountries: []
                    },
                    apis: {
                        custom: {
                            endpoint: {
                                uri: prodLambda
                            },
                            interfaces: [
                                {
                                    type: 'CAN_FULFILL_INTENT_REQUEST',
                                },
                                {
                                    type: 'ALEXA_PRESENTATION_APL'
                                }
                            ]
                        }
                    },
                    permissions: [
                        {
                            name: 'alexa::devices:all:notifications:write'
                        }
                    ],
                    "privacyAndCompliance": {
                        "allowsPurchases": false,
                        "usesPersonalInfo": false,
                        "isChildDirected": false,
                        "isExportCompliant": true,
                        "containsAds": false,
                        "locales": {
                            "en-US": {
                                "privacyPolicyUrl": "http://voicefirsttech.com/my-dry-cleaner/privacy-policy.pdf",
                                "termsOfUseUrl": "https://voicefirsttech.com/my-dry-cleaner/terms-of-use.pdf"
                            }
                        }
                    },
                },
            },
            googleAction: {
                nlu: 'dialogflow',
                dialogflow:{
                    projectId: "voicefirsttech-mybusiness",
                    keyFile: "voicefirsttech-mybusiness-dialogflow.json",
                    agent: {
                        webhook:{
                            url:googleApiGatewayUrl
                        }
                    }
                }      
                
            }
        }
    }
    
};