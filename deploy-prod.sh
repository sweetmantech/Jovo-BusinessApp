#!/bin/bash
# ONLY RUN FROM LOCAL MACHINE
# This script updates the provided app
# Updates Skill Model
# Updates Skill Manifest (info)
# Updates Prod Lambda
# run with the command:
# sudo sh deploy-prod.sh "<name-of-cleaner>"
python sheets.py -b "$1"
jovo build -d --stage prod